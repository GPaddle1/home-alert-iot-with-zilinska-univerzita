# Home alert IOT with Žilinská univerzita

The full project for the end of the IOT lessons with Žilinská univerzita

Here we have a monitoring app to get all the important data ready to use

## Use cases

### Bathroom:

> Just 5 more minutes

You always say this, but now, you will get an alert when you take a too long shower. The planet will say you thank you!

### Office:

Your environment is getting darker and darker, let the ESP inform you that it is time to add some light into your office!

### Daily recap:

Each day you will get a daily recap at 12:00 at the mentionned mail address on the .env file

## How to setup the project

### Node-red

First run the `docker-compose.yml` with the following commands:

```bash
cp .env.example .env
# Setup the environement variables
npm install node-red-data/
docker-compose -p ZilinaIOT up -d
```

And voila!

Your node-red instance is by default configure to run with `admin` / `pass` and for the MQTT broker: `iot` / `123456789`

> Be careful to have the 1880 and 1883 ports available or change these on both the `docker-compose.yml` and the nodes

You can change the users [by following the node red doc](https://nodered.org/docs/user-guide/runtime/securing-node-red#usernamepassword-based-authentication)

### Python

For setting up the whole system you need to define into `credentials.py` your personal information as defined in `credentials.example.py`

Using Thonny you have to upload the files to the board then run the main file then all should be ready.

You can adjust your refreshing rate on the `main.py`, the time is in millisecond.

### Network

The board will automatically try to connect for 10s to the first registered network.

If the connection failed, you will get a buzzer sound to describe it.

## Data

Data are stored into the `node-red-data/files/` directory, you will find a list of the alerts and the registered data in CSV file format.

> Take care to not log too much information, otherwise you will get a really big file which will slow the whole project down. A daily purge should be good if you keep the default 1s data collection

## What to use

MQTT routes to register to:

- /IOT/alert/+/format
- /IOT/alert/office/format
- /IOT/alert/bathroom/format

With these alerts you will get information about how to improve your life quality

With an app like [MQTT Dashboard for Android](https://play.google.com/store/apps/details?id=com.app.vetru.mqttdashboard) you can get push notification with these routes

## How to change the place where you log from

You can switch from office to bathroom data logging with the node red UI interface that is available at this address for localhost with the default parameters: [http://localhost:1880/ui](http://localhost:1880/ui)

On the UI you can also get relevant information grouped by situation or add sound to the alerts!

## Troubleshooting

For security reasons, the credentials are not shared, you could have troubles with the mailer, if so, you just need to change on the `main program` flow the value of `Send daily recap to` / UserId = `${MAIL_FROM}` and Password = `${MAIL_PASS}`

## Credits

Keller Guillaume @CNAM FIP2A w/ Žilinská univerzita 2022
