from LED import LED
from Darkness import Darkness
from HumidityTemperature import HumidityTemperature
from AirQuality import AirQuality

# led = LED()
# led.demo()

darkness = Darkness()
print(darkness.getValueInPercent())

humidityTemperature = HumidityTemperature()
print(humidityTemperature.getValue())

airQuality = AirQuality()
print(airQuality.getValueInPercent())
