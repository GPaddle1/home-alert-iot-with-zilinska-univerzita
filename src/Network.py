from Buzzer import Buzzer
import network
import time

from credentials import Wifi

waitingDelay_miliseconds = 500
MILISECONDS_TO_SECONDS = 1000

maxDuration_seconds = 10

maxTries = maxDuration_seconds / waitingDelay_miliseconds * MILISECONDS_TO_SECONDS

knownWifi = []
wlan = network.WLAN(network.STA_IF)


def fillAvailableNetwork():
    print("Scanning for WiFi networks, please wait...\n")

    wlan.active(True)

    availableWifi = wlan.scan()

    for (ssid, bssid, channel, RSSI, authmode, hidden) in availableWifi:

        prefix = "x"
        suffix = "Unknown"

        SSIDAsString = "{:s}".format(ssid)
        if SSIDAsString in Wifi:
            prefix = "-->"
            suffix = "Ok"
            knownWifi.append(SSIDAsString)

        print(prefix, "\t\t", SSIDAsString, "\t\t", suffix)

    print("You can connect to :")
    print(knownWifi)


def connectToFirstWifi():

    if not wlan.isconnected():
        print("Let's connect")
        fillAvailableNetwork()
    else:
        print("Already connected to", wlan.config("essid"))

    for ssid in knownWifi:
        wlan.connect(ssid, Wifi[ssid])
        for i in range(maxTries):
            if not wlan.isconnected():
                time.sleep_ms(waitingDelay_miliseconds)
            else:
                print("Connection to", ssid, "succeded")
                break

        if wlan.isconnected():
            break
        else:
            print("Connection failed for", ssid)

    print("Network config:", wlan.ifconfig())


def connectToWifi(ssid, password):

    wlan.connect(ssid, password)
    for i in range(maxTries):
        if not wlan.isconnected():
            time.sleep_ms(waitingDelay_miliseconds)
        else:
            print("Connection to ", ssid, " succeded")
            break
    if not wlan.isconnected():
        print("A problem occured during the connection please retry")
        buzzer = Buzzer()
        for i in range(10):
            buzzer.play([880 + i * 50], 0.15)

        return
    print("network config:", wlan.ifconfig())
