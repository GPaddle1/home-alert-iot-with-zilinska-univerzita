def singleton(cls):
    instance = None

    def getinstance(*args, **kwargs):
        nonlocal instance
        if instance is None:
            instance = cls(*args, **kwargs)
        return instance

    return getinstance


@singleton
class HumidityTemperature:
    def __init__(self):
        from machine import Pin
        from dht import DHT11

        global sensor
        sensor = DHT11(Pin(25))

    def getTemperatureValue(self):
        try:
            sensor.measure()
            return {"temperature": sensor.temperature(), "unit": "°C"}
        except OSError as e:
            print("Failed to read sensor. ErrNo : " + str(e.errno))

    def getHumidityValue(self):
        try:
            sensor.measure()
            return {"humidity": sensor.humidity(), "unit": "%"}
        except OSError as e:
            print("Failed to read sensor. ErrNo : " + str(e.errno))

    def getValue(self):
        try:
            sensor.measure()
            return {
                "humidity": {"value": sensor.humidity(), "unit": "%"},
                "temperature": {"value": sensor.temperature(), "unit": "°C"},
            }

        except OSError as e:
            print("Failed to read sensor. ErrNo : " + str(e.errno))
