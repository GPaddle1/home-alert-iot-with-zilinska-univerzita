Wifi = {
    "SSID1": "PassWord1",
    "SSID2": "PassWord2",
    "SSID3": "PassWord3",
    "SSID4": "PassWord4",
}

MQTT = {
    "CLIENT_ID": "<Your name>",
    "MQTT_BROKER_IP": "<Broker IP>",
    "MQTT_USER": "<broker username>",
    "MQTT_PASSWORD": "<Your password>",
    "PORT": 1883,
}
