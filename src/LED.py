def singleton(cls):
    instance = None

    def getinstance(*args, **kwargs):
        nonlocal instance
        if instance is None:
            instance = cls(*args, **kwargs)
        return instance

    return getinstance


@singleton
class LED:
    STEP_DELAY_MS = 40
    FREQ = 900
    DUTY = 500

    def __init__(self):
        from machine import Pin, PWM
        import time
        import math

        global led_red, led_green, led_blue

        led_red = PWM(Pin(21), freq=self.FREQ, duty=0)
        led_green = PWM(Pin(23), freq=self.FREQ, duty=0)
        led_blue = PWM(Pin(22), freq=self.FREQ, duty=0)

    def lightUp(self, redDuty, greenDuty, blueDuty, brightness=100):

        led_red.duty((int)(redDuty * brightness / 100))
        led_green.duty((int)(greenDuty * brightness / 100))
        led_blue.duty((int)(blueDuty * brightness / 100))

    def lightDown(self):

        led_red.duty(0)
        led_green.duty(0)
        led_blue.duty(0)

    def end(self):

        led_red.deinit()
        led_green.deinit()
        led_blue.deinit()

    def demo(self,CYCLES=2):

        import time
        import math

        global led_red, led_green, led_blue
        print(self.STEP_DELAY_MS)

        def pulse(self, led_red, led_green, led_blue, t):
            for i in range(0, 60):
                redDuty = int(math.sin((i + 0) / 30 * math.pi) * self.DUTY + self.DUTY)
                greenDuty = int(math.sin((i + 20) / 30 * math.pi) * self.DUTY + self.DUTY)
                blueDuty = int(math.sin((i + 40) / 30 * math.pi) * self.DUTY + self.DUTY)

                self.lightUp(redDuty, greenDuty, blueDuty)
                time.sleep_ms(t)

        for i in range(CYCLES):
            pulse(self, led_red, led_green, led_blue, self.STEP_DELAY_MS)

        led_red.deinit()
        led_green.deinit()
        led_blue.deinit()