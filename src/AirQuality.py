def singleton(cls):
    instance = None

    def getinstance(*args, **kwargs):
        nonlocal instance
        if instance is None:
            instance = cls(*args, **kwargs)
        return instance

    return getinstance


@singleton
class AirQuality:
    def __init__(self):
        from machine import I2C
        from sgp40 import SGP40

        global i2c
        i2c = I2C(0)
        global sgp40
        sgp40 = SGP40(i2c, 0x59)

    def getValue(self):
        return {"airQuality": sgp40.measure_raw(), "unit": "none"}

    def getValueInPercent(self):
        from Utilities import convert

        valueInPercent = convert(self.getValue()["airQuality"], 0, 65535, 0, 100)
        return {"airQuality": valueInPercent, "unit": "%"}
