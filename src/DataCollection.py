from time import sleep_ms

import ujson

from LED import LED
from AirQuality import AirQuality
from Darkness import Darkness
from HumidityTemperature import HumidityTemperature
from Buzzer import Buzzer

from umqtt.simple import MQTTClient

topicSuffix = ""
sound = False


def getDataAsObject():
    humidityTemperature = HumidityTemperature().getValue()

    temperature = humidityTemperature["temperature"]
    humidity = humidityTemperature["humidity"]
    airQuality = AirQuality().getValueInPercent()
    darkness = Darkness().getValueInPercent()

    message = {
        "temperature": temperature["value"],
        "humidity": humidity["value"],
        "airQuality": airQuality["airQuality"],
        "darkness": darkness["darkness"],
    }

    return message


def sendSensorDataToNodeRed():
    global client
    global topicSuffix
    message = getDataAsObject()
    jsMsg = ujson.dumps(message)
    topic = "/IOT/data" + topicSuffix
    client.publish(topic=topic, msg=jsMsg)
    print("Data published", message, "At topic", topic)


def sub_callback(topic, msg):
    global topicSuffix
    global sound
    # print("Topic = ", topic, "msg = ", msg)
    value = ujson.loads(msg)

    if topic == b"/IOT/setup/topic":
        topicSuffix = value["topic"]
        print("Topic set to", topicSuffix)
        return

    if topic == b"/IOT/setup/sound":
        sound = value["state"]
        print("Sound set to", sound)
        return

    nbPulse = value["bip"]
    red, green, blue = value["red"], value["green"], value["blue"]

    led = LED()

    for i in range(nbPulse):
        led.lightUp(red, green, blue)
        sleep_ms(200)
        led.lightDown()
        sleep_ms(200)

    if sound:
        buzzer = Buzzer()
        for i in range(nbPulse):
            buzzer.play([880 + i * 50], 0.15)


def sendToNodeRed(DELAY_MS=10000):
    from credentials import MQTT

    global client

    client = MQTTClient(
        client_id=MQTT["CLIENT_ID"],
        server=MQTT["MQTT_BROKER_IP"],
        port=MQTT["PORT"],
        user=MQTT["MQTT_USER"],
        password=MQTT["MQTT_PASSWORD"],
    )
    client.connect()

    client.set_callback(sub_callback)

    client.subscribe("/IOT/alert/+/technic", qos=1)
    client.subscribe("/IOT/setup/topic", qos=1)
    client.subscribe("/IOT/setup/sound", qos=1)

    while True:
        try:
            client.check_msg()
            sendSensorDataToNodeRed()
        except OSError:
            print("Failed to read sensor")
        sleep_ms(DELAY_MS)
