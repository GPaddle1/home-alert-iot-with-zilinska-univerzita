def singleton(cls):
    instance = None

    def getinstance(*args, **kwargs):
        nonlocal instance
        if instance is None:
            instance = cls(*args, **kwargs)
        return instance

    return getinstance


@singleton
class Darkness:
    def __init__(self):
        from machine import Pin

        global pin
        pin = Pin(34)

    def getValue(self):
        from machine import ADC

        adc = ADC(pin)
        adc.atten(ADC.ATTN_11DB)

        return {"darkness": adc.read_u16(), "unit": "none"}

    def getValueInPercent(self):
        from Utilities import convert

        valueInPercent = convert(self.getValue()["darkness"], 0, 65535, 0, 100)
        return {"darkness": valueInPercent, "unit": "%"}
