def convert(value, inputMin, inputMax, outputMin, outputMax):
    return max(
        min(
            outputMax,
            (value - inputMin) * (outputMax - outputMin) // (inputMax - inputMin)
            + outputMin,
        ),
        outputMin,
    )
